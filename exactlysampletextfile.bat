java -Xmx2048m -cp "%~dp0bin\;%~dp0dist\*;%~dp0lib\*;" ^
	edu.northwestern.at.morphadorner.tools.sampletextfile.ExactlySampleTextFile ^
	"%1" "%2" "%3"
