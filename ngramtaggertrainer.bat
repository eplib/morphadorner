java -Xmx4096m -Xss1m -cp "%~dp0bin\;%~dp0dist\*;%~dp0lib\*;" edu.northwestern.at.morphadorner.tools.taggertrainer.ngram.NGramTaggerTrainer ^
	"%1" "%2" "%3" "%4" "%5" "%6" "%7" "%8" "%9"
