java -Xmx2048m -Xss1m -cp "%~dp0bin\;%~dp0dist\*;%~dp0lib\*" ^
	edu.northwestern.at.morphadorner.tools.xmltotab.XMLToTab "%1" "%2"
