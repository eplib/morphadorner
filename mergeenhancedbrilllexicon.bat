java -Xmx2048m -cp "%~dp0bin\;%~dp0dist\*;%~dp0lib\*;" ^
	edu.northwestern.at.morphadorner.tools.mergeenhancedbrilllexicon.MergeEnhancedBrillLexicon ^
	"%1" "%2" "%3"
