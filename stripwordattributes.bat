java -Xmx2048m -Xss1m -cp "%~dp0bin\;%~dp0dist\*;%~dp0lib\*;" ^
	edu.northwestern.at.morphadorner.tools.stripwordattributes.StripWordAttributes ^
	"%1" "%2" "%3" "%4" "%5"
